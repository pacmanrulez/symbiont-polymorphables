<?php

namespace Symbiont\Polymorphables\Relations;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;
use Symbiont\Polymorphables\Relations\Pivot\PolymorphablePivot;

abstract class Polymorphic {

    /**
     * @var string[]
     */
    public static $polyMorphMap = [];

    public static function query() {
        return DB::table((new PolymorphablePivot)->getTable());
    }

    /**
     * @param string $method
     * @return string|bool
     */
    public static function methodHasPolymorphicRelation(string $method) : string|bool {
        $alias = self::getAliasFromMethod($method);

        return Polymorphic::hasPolyAlias($alias) ?
            $alias : false;
    }

    public static function getAliasFromClass($class) {
        $map = array_flip(Relation::$morphMap);
        return array_key_exists($class, $map)
                ? $map[$class] : false;
    }

    /**
     * @param string $alias
     * @return bool
     */
    public static function hasPolyAlias(string $alias) : bool {
        return array_key_exists($alias, static::$polyMorphMap) || array_key_Exists($alias, Relation::$morphMap);
    }

    /**
     * @param string $alias
     * @return string|null
     */
    public static function getPolyModel(string $alias) : string|null {
        return Relation::$morphMap[$alias] ?? null;
    }

    /**
     * @param string $class
     * @return false|int|string
     */
    public static function findPolyModel($class) {
        return Relation::getMorphedModel(is_object($class) ? $class::class : $class);
    }

    /**
     * @param $method
     * @return string
     */
    public static function getAliasFromMethod($method) {
        return str($method)
            ->singular()
            ->toString();
    }

}