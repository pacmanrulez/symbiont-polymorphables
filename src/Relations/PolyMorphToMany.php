<?php

namespace Symbiont\Polymorphables\Relations;

use App\Contracts\Poly\Polymorphable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Concerns\InteractsWithPivotTable;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Facades\DB;
use Symbiont\Polymorphables\Relations\Pivot\PolymorphablePivot;

class PolyMorphToMany extends MorphToMany
{

    protected array $usingPivotGroups = [];

    protected array|null $usingGroups = null;

    use InteractsWithPivotTable {
        InteractsWithPivotTable::attach as public originalAttach;
        InteractsWithPivotTable::detach as public originalDetach;
        InteractsWithPivotTable::newPivotQuery as public originalNewPivotQuery;
    }

    public function __construct(Builder $query, Model $parent, $name, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relationName = null, $inverse = false)
    {
        parent::__construct($query, $parent, $name, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relationName, $inverse);

        $this->pivotColumns = PolymorphablePivot::getSelectableFields();
    }

    /**
     * @param $group
     * @return PolyMorphToMany
     */
    public function group(...$groups)
    {
        $this->usingPivotGroups = collect($groups)->filter()->toArray();
        $this->setWherePivotGroupClause();
        return $this;
    }

    public function withGroup() {
        $this->wherePivot(PolymorphablePivot::ATTR_GROUP, 'is not', DB::raw('null'));
        return $this;
    }

    public function noGroup() {
        $this->wherePivot(PolymorphablePivot::ATTR_GROUP, 'is', DB::raw('null'));
        return $this;
    }

    protected function setWherePivotGroupClause() {
        $this->wherePivotIn(PolymorphablePivot::ATTR_GROUP, $this->usingPivotGroups);
    }

    protected function replaceIdWithType($value) {
        return str_replace('_id', '_type', $value);
    }

    /**
     * Set the join clause for the relation query.
     *
     * @param \Illuminate\Database\Eloquent\Builder|null $query
     * @return $this
     */
    protected function performJoin($query = null)
    {
        $query = $query ?: $this->query;

        // We need to join to the intermediate table on the related model's primary
        // key column with the intermediate table's foreign key for the related
        // model instance. Then we can set the "where" for the parent models.
        $query->join($this->table, function ($join) {
            $join
                ->on(
                    $this->getQualifiedRelatedKeyName(),
                    '=',
                    $this->getQualifiedRelatedPivotKeyName()
                )
                ->where(
                // ($this->inverse ? 'content' : 'model') . '_type'
                    $this->table . '.' . $this->replaceIdWithType($this->relatedPivotKey),
                    '=',
                    $this->inverse ? $this->morphClass : $this->related->getMorphClass()
                );
        });

        return $this;
    }

    /**
     * Set the where clause for the relation query.
     *
     * @return $this
     */
    protected function addWhereConstraints()
    {
        // we are skipping MorphToMany parent class calling BelongsToMany directly
        BelongsToMany::addWhereConstraints();

        // here we exchange `$this->morphClass` (which is $this->related->morphClass()) with `$this->parent->getMorphClass()`
        $this->query->where(
            $this->qualifyPivotColumn($this->morphType),
            $this->parent->getMorphClass()
        );

        return $this;
    }


    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param array $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        // we are skipping MorphToMany parent class calling BelongsToMany directly
        BelongsToMany::addEagerConstraints($models);

        // here we exchange `$this->morphClass` (which is $this->related->morphClass()) with `$this->parent->getMorphClass()`
        $this->query->where($this->qualifyPivotColumn($this->morphType), $this->parent->getMorphClass());
    }


    /**
     * @return \Illuminate\Database\Query\Builder
     * @used [detach]
     */
    public function newPivotQuery()
    {
        return $this->originalNewPivotQuery()
            ->where($this->replaceIdWithType($this->relatedPivotKey), $this->related->getMorphClass())
            ->where($this->morphType, $this->parent->getMorphClass());

    }

    /**
     * @param $id
     * @param array $attributes
     * @param $touch
     * @return void
     */
    public function attach($id, array $attributes = [], $touch = true)
    {
        if($this->usingPivotGroups) {
            $attributes = array_merge($attributes, [
                PolymorphablePivot::ATTR_GROUP => $this->usingPivotGroups[0]
            ]);
        }

        // insert into `contents` (`content_id`, `model_id`) values (10, 1)
        $this->originalAttach($id, array_merge($attributes, [
            $this->replaceIdWithType($this->relatedPivotKey) => $this->related->getMorphClass(),
            $this->morphType => $this->parent->getMorphClass()
        ]));

    }

    public function detach($ids = null, $touch = true)
    {
        if($this->usingPivotGroups) {
            $this->setWherePivotGroupClause();
        }

        return $this->originalDetach($ids, $touch);
    }

}