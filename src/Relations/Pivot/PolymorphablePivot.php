<?php

namespace Symbiont\Polymorphables\Relations\Pivot;

use Illuminate\Database\Eloquent\Relations\MorphPivot;
use Symbiont\Utilizer\Concerns\HandlesGroups;
use Symbiont\Utilizer\Concerns\HandlesSequences;
use Symbiont\Utilizer\Contracts\Groupable;
use Symbiont\Utilizer\Contracts\Sequenceable;

class PolymorphablePivot extends MorphPivot implements Sequenceable, Groupable {

    use HandlesSequences,
        HandlesGroups;

    const ATTR_ID = 'id';
    const ATTR_MODEL_ID = 'model_id';
    const ATTR_MODEL_TYPE = 'model_type';
    const ATTR_CONTENT_ID = 'content_id';
    const ATTR_CONTENT_TYPE = 'content_type';

    public $incrementing = true;

    protected $table = 'polymorphables';

    protected $fillable = [
        self::ATTR_ID, // id?
        self::ATTR_MODEL_ID,
        self::ATTR_MODEL_TYPE,
        self::ATTR_CONTENT_ID,
        self::ATTR_CONTENT_TYPE
    ];

    public static function getSelectableFields(): array {
        return [
            self::ATTR_MODEL_ID, self::ATTR_MODEL_TYPE,
            self::ATTR_CONTENT_ID, self::ATTR_CONTENT_TYPE,
            self::ATTR_GROUP, self::ATTR_SEQUENCE
        ];
    }

}