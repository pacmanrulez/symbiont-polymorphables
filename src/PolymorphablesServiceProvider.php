<?php

namespace Symbiont\Polymorphables;

use Illuminate\Support\ServiceProvider;

class PolymorphablesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
         $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('polymorphables.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'polymorphables');

        // Register the main class to use with the facade
        $this->app->singleton('polymorphables', function () {
            return new Polymorphables;
        });
    }
}
