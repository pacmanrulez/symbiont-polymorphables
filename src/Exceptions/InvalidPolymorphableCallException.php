<?php

namespace Symbiont\Polymorphables\Exceptions;

class InvalidPolymorphableCallException extends \Exception {

    public function __construct($parent, $related, $method)
    {
        parent::__construct("Invalid relation requested `".$parent."` using relation method {$method}. Add `".($related ?? $method)."` class to the `".$parent."::\$polymorphables` array.");
    }

}