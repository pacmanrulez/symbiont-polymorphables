<?php

namespace Symbiont\Polymorphables\Exceptions;

class PolymorphablSelfAssignException extends \Exception {

    public function __construct($class)
    {
        parent::__construct("Not allowed to self assign class {$class}");
    }

}