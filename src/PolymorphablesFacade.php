<?php

namespace Symbiont\Polymorphables;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Symbiont\Polymorphables\Skeleton\SkeletonClass
 */
class PolymorphablesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'polymorphables';
    }
}
