<?php

namespace Symbiont\Polymorphables\Models;

use Symbiont\Polymorphables\Concerns\HandlesPolyMorphic;

use Illuminate\Database\Eloquent\Model;
use Symbiont\Polymorphables\Contracts\Polymorphable;

class PolymorphicModel extends Model implements Polymorphable {

    use HandlesPolyMorphic;

    protected $hidden = [
        'pivot'
    ];

}