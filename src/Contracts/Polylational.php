<?php

namespace Symbiont\Polymorphables\Contracts;

use Illuminate\Support\Collection;

interface Polylational {

    public function polymorphables();

}