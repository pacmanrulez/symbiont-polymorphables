<?php

namespace Symbiont\Polymorphables\Concerns;

use Illuminate\Database\Eloquent\Model;

trait VerifiesEloquentModel {

    public static function bootVerifiesEloquentModel() {
        if(! is_a(static::class, Model::class, true)) {
            throw new \Exception('trait ' . __TRAIT__ .' can only be applied on Eloquent models.');
        }
    }

}