<?php

namespace Symbiont\Polymorphables\Concerns;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

use Illuminate\Database\Eloquent\Relations\Relation;
use Symbiont\Polymorphables\Contracts\Polymorphable;
use Symbiont\Polymorphables\Exceptions\InvalidPolymorphableCallException;
use Symbiont\Polymorphables\Exceptions\PolymorphablSelfAssignException;
use Symbiont\Polymorphables\Relations\Pivot\PolymorphablePivot;
use Symbiont\Polymorphables\Relations\Polymorphic;
use Symbiont\Polymorphables\Relations\PolyMorphToMany;

trait HandlesPolyMorphic {
    
    /**
     * @var array|string[]
     * ['*'] == accept all polymorphables by default
     */
    protected array $polymorphables = ['*'];

    protected bool $accepts_plain = true;

    protected bool $accepts_self = true;

    protected bool $poly_morphed = false;

    use HasAttributes {
        HasAttributes::isRelation as public originalIsRelation;
        HasAttributes::getRelationshipFromMethod as protected originalGetRelationshipFromMethod;
    }
    use HasRelationships {
        HasRelationships::newMorphToMany as protected originalNewMorphToMany;
    }
    use VerifiesEloquentModel;

    /**
     * @param Builder $query
     * @param Model $parent
     * @param $name
     * @param $table
     * @param $foreignPivotKey
     * @param $relatedPivotKey
     * @param $parentKey
     * @param $relatedKey
     * @param $relationName
     * @param $inverse
     * @return MorphToMany
     */
    public function newMorphToMany(Builder $query, Model $parent, $name, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relationName = null, $inverse = false) : MorphToMany
    {
        return new PolyMorphToMany($query, $parent, $name, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey,
            $relationName, $inverse);
    }

    /**
     * @param $key
     * @return bool
     */
    public function isRelation($key) {
        return Polymorphic::methodHasPolymorphicRelation($key) ||
            $this->originalIsRelation($key);
    }

    /**
     * @param $method
     * @return \Illuminate\Support\HigherOrderTapProxy|mixed|void
     */
    protected function getRelationshipFromMethod($method) {

        $relation = $this->polyMorphMethodCall($method);
        $singular = $method === str($method)->singular()->value;

        if(is_null($relation)) {
            return $this->originalGetRelationshipFromMethod($method);
        }

        if($singular) {
            $relation->orderByPivot('sequence');
        }
        $tapped = tap($relation->getResults(), function ($results) use ($method) {
            $this->setRelation($method, $results);
        });
        return $singular ? $tapped->first() : $tapped;
    }

    /**
     * @param $related
     * @param $name
     * @param $table
     * @param $foreignPivotKey
     * @param $relatedPivotKey
     * @param $parentKey
     * @param $relatedKey
     * @param $relation
     * @param $inverse
     * @return MorphToMany
     *
     * // inverse
     */
    public function polyMorphToMany($related, $name, $table = null, $foreignPivotKey = null,
                                     $relatedPivotKey = null, $parentKey = null,
                                     $relatedKey = null, $relation = null) : MorphToMany {
        // inverse = false
        return $this->morphToMany($related, $name, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relation)
                ->using(PolymorphablePivot::class);
    }

    /**
     * @param $related
     * @param $name
     * @param $table
     * @param $foreignPivotKey
     * @param $relatedPivotKey
     * @param $parentKey
     * @param $relatedKey
     * @param $relation
     * @return MorphToMany
     */
    public function polyMorphedByMany($related, $name, $table = null, $foreignPivotKey = null,
                                      $relatedPivotKey = null, $parentKey = null, $relatedKey = null, $relation = null) : MorphToMany {
        // inverse = true
        return $this->morphedByMany($related, $name, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relation)
            ->using(PolymorphablePivot::class);
    }

    /**
     * @param $method
     * @return mixed
     */
    protected function polyMorphMethodCall($method, $caller = null) {
        $alias = str($method)->singular()->toString();

        $singular = $alias === $method;
        $class = Polymorphic::getPolyModel($alias) ?? Relation::getMorphedModel($alias);

        // $instance = app($class);

//        // set morphed from this instance
//        $morphed = $this->isPolyMorphed();
//
//        // given instance is not polymorphable
//        if(! $instance instanceof Polymorphable) {
//            // doesn't mather if $this is morphed, $instance isn't polymorphable so
//            // we'll set this class as model_* and $instance as content_*
//            $morphed = true;
//        }
//        // if both are morphed (content_*), use general rule of natural sort order
//        else {
//            if($instance->isPolyMorphed() && $morphed) {
//                $morphed = $class > $this::class;
//            }
//        }

        // this line can replace the above
        $morphed = $class > $this::class;

        return $this->getMorphMethod($class, $morphed);
    }

    /**
     * @param $class
     * @param $morph
     * @return MorphToMany
     */
    protected function getMorphMethod($class, $morph = false) {
        $table = (new PolymorphablePivot)->getTable();
        return $morph ?
            $this->polyMorphedByMany($class, 'model', $table, PolymorphablePivot::ATTR_MODEL_ID, PolymorphablePivot::ATTR_CONTENT_ID, 'id', 'id') :
            $this->polyMorphToMany($class, 'content', $table, PolymorphablePivot::ATTR_CONTENT_ID, PolymorphablePivot::ATTR_MODEL_ID, 'id', 'id');
    }

    /**
     * @param $method
     * @param $parameters
     * @return mixed
     * @throws InvalidPolymorphableCallException
     */
    public function __call($method, $parameters) {
        $relation_method = str($method)->snake()->value;

        $alias = Polymorphic::methodHasPolymorphicRelation($relation_method);
        if(! $alias ) {
            return parent::__call($method, $parameters);
        }

        if(! $this->acceptsObjectRelation($alias)) {
            $class = Polymorphic::getPolyModel($alias);
            throw new InvalidPolymorphableCallException($this::class, $class, $relation_method);
        }

        return $this->polyMorphMethodCall($relation_method, $this->getCaller());
    }

    /**
     * Try to trace back the caller object if trace of any reference to $parent or
     * $related gets lost
     * @return object|mixed|null
     */
    protected function getCaller(): ?object {
        foreach (debug_backtrace() as $call) {
            if(isset($call['object'])) {
                return $call['object'];
            }
        }

        return null;
    }

    /**
     * @param $alias
     * @return bool
     * @throws PolymorphablSelfAssignException
     */
    protected function acceptsObjectRelation($alias) : bool {
        if(in_array('*', $this->polymorphables)) {
            return true;
        }

        if(! ($class = Polymorphic::getPolyModel($alias)) ) {
            return false;
        }
        if($class === $this::class && !$this->accepts_self) {
            throw new PolymorphablSelfAssignException($class);
        }

        $instance = app($class);
        if(! $instance instanceof Polymorphable) {
            return $this->accepts_plain;
        }

        return in_array($class, $this->polymorphables);
    }

    /**
     * @return string|null
     */
    public function getPolyClass() : string|null {
        return Polymorphic::findPolyModel(static::class) ?? null;
    }

    // determines if to use MorphedByMany or MorphToMany.
    public function isPolyMorphed() : bool {
        return $this->poly_morphed;
    }

    protected function acceptsPolymorphableClass($class) : bool {
        return (bool) count(array_intersect(['*', $class], $this->polymorphables)) ?? false;
    }

    /**
     * Find all relations attached to this object
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function withAttached() {
        return self::query()
            ->with(self::getAttachedPolys($this->getMorphClass())->toArray())
            ->where('id', $this->id);
    }

    /**
     * Find all relations attach to this class
     * @param $morph
     * @return mixed
     */
    protected static function getAttachedPolys($morph) {
        return Polymorphic::query()
            ->select(PolymorphablePivot::ATTR_MODEL_TYPE)
            ->distinct()
            ->where(PolymorphablePivot::ATTR_CONTENT_TYPE, $morph)
            ->get()->map(function($entry) {
                return str($entry->model_type)->plural()->toString();
            })->filter();
    }

}