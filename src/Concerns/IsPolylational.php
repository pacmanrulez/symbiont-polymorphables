<?php

namespace Symbiont\Polymorphables\Concerns;

use Illuminate\Database\ClassMorphViolationException;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use Symbiont\Polymorphables\Relations\Pivot\PolymorphablePivot;
use Symbiont\Polymorphables\Relations\Polymorphic;

trait IsPolylational {

    use VerifiesEloquentModel;

    /**
     * @param string ...$classes
     * @return \Illuminate\Support\Collection
     */
    public function polymorphable(string ...$classes) : Collection {
        $query = $this->polymorphables()->where(function($query) use ($classes) {
            foreach($classes as $class) {
                if(! $alias = Polymorphic::getAliasFromClass($class)) {
                    throw new ClassMorphViolationException(app($class));
                }

                $query->orWhere(PolymorphablePivot::ATTR_MODEL_TYPE, $alias);
            }
        });

        return $this->handlePolyResults($query);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function polymorphables() : Builder {
        return DB::table((new PolymorphablePivot)->getTable())
            ->where(PolymorphablePivot::ATTR_CONTENT_ID, $this->id)
            ->where(PolymorphablePivot::ATTR_CONTENT_TYPE, $this->getMorphClass())
            ->orderBy('id');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getPolymorphablesAttribute() {
        return $this->handlePolyResults($this->polymorphables()->get());
    }

    /**
     * Very expensive retrieval of models, just for POC
     *
     * @param $collection
     * @return \Illuminate\Support\Collection
     */
    protected function handlePolyResults($collection) {
        $models = collect([]);
        $collection->get()->map(function($entry) use ($models) {
            $class = Relation::getMorphedModel($entry->{PolymorphablePivot::ATTR_MODEL_TYPE});
            try {
                $models->add($class::findOrFail($entry->{PolymorphablePivot::ATTR_MODEL_ID}));
                // ignore
            } catch(\Exception) {}
        });
        return $models;
    }

}