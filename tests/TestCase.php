<?php

namespace Symbiont\Polymorphables\Tests;

use Orchestra\Testbench\TestCase as OrchestraTestCase;

use Symbiont\Polymorphables\PolymorphablesServiceProvider;

class TestCase extends OrchestraTestCase {

    protected function getPackageProviders($app)
    {
        return [
            PolymorphablesServiceProvider::class,
        ];
    }

    /*
    protected function getEnvironmentSetUp($app) {
        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', [
            'driver' => 'sqlite',
            'host' => '127.0.0.1',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }

    protected function defineDatabaseMigrations() {
        $this->loadLaravelMigrations();

        $this->loadMigrationsFrom(dirname(__DIR__).'/src/database/migrations');
    }
    */
}
