# Examples

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

<!-- this is a comment -->

<!-- this is a section line -->

----

## This is a second header

With some text

### This is a third header

Another text

#### This is a fourth header

More text

##### This is a fifth header

Fifth text again

###### This is a sixth header

Still some text

## Code examples

### Inline

This is an inline `code` block where `public $variable = 'some-string';` and may help in solving a problem.

### Block

``` php title="app/Symbiont/Core/Example.php"
<?php
/** This is a PHP section */
namespace Symbiont\Core;

class Example extends BaseExample implements Exampable {

    protected string|null $value = null;

    public function __construct(int $value): void {
        $this->value = $value;
    }

}
```

``` html+twig title="example.blade.php"
<x-component :message="$something" :class="bliep">
    {{ $slot }}
    This is suppose to be bladesyntx
</x-component>
```

``` py title="bubble_sort.py"
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```

## Tabs

=== "First"

    This is the first tabs content

=== "Second"

    This is the second tabs content

## Tabs with code blocks

=== "Example"

    This is an example on how to create an example, this isn't very much
    but it's just an example to show how this example with a little example
    notification.

    !!! note
      
        And this should be a note about the example on how to use the example

=== "C"

    ``` c
    #include <stdio.h>

    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "C++"

    ``` c++
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```

=== "PHP"

    ``` php
    <?php
    use Utilities\IOStream;
  
    IOStream::main(): void {
       self::log("Hello world!");
    }
    ```

## Table

| Center  |         Left         |               Right |
|:-------:|:--------------------:|--------------------:|
|  `GET`  |   :material-check:   |      Fetch Resource |
|  `PUT`  | :material-check-all: |     Update Resource |
| `DELETE` |   :material-close:   |     Delete Resource |

## Grid

<div class="grid cards" markdown>

- :fontawesome-brands-html5: __HTML__ for content and structure
- :fontawesome-brands-js: __JavaScript__ for interactivity
- :fontawesome-brands-css3: __CSS__ for text running out of boxes
- :fontawesome-brands-internet-explorer: __Internet Explorer__ ... huh?

</div>

## Notes

!!! note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! warning

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! example

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! quote

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

---

## Left or right positioned notes

!!! info inline end "Lorem ipsum"

    Lorem ipsum dolor sit amet, consectetur
    adipiscing elit. Nulla et euismod nulla.
    Curabitur feugiat, tortor non consequat
    finibus, justo purus auctor massa, nec
    semper lorem quam in massa.

Everything written here will be on the left and the infobox will be right from this text.
This is very usefull to structure small parts of information unwillingly using blocks. Everything written here will be on the left and the infobox will be right from this text.
This is very usefull to structure small parts of information unwillingly using blocks. Everything written here will be on the left and the infobox will be right from this text.
This is very usefull to structure small parts of information unwillingly using blocks.
Everything written here will be on the left and the infobox will be right from this text.
This is very usefull to structure small parts of information unwillingly using blocks.
Everything written here will be on the left and the infobox will be right from this text.
This is very usefull to structure small parts of information unwillingly using blocks.

___

## Checklist

- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque

---

[![Built with Material for MkDocs](https://img.shields.io/badge/Material_for_MkDocs-526CFE?style=for-the-badge&logo=MaterialForMkDocs&logoColor=white)](https://squidfunk.github.io/mkdocs-material/)