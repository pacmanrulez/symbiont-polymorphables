<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Symbiont\Polymorphables\Relations\Pivot\PolymorphablePivot;

use Symbiont\Utilizer\Utilizer;
use Symbiont\Utilizer\Utilities\{Sequencer, Grouper};

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create((new PolymorphablePivot)->getTable(), function (Blueprint $table) {
            $char_length = 50;

            $table->id();
            $table->bigInteger(PolymorphablePivot::ATTR_MODEL_ID);
            $table->string(PolymorphablePivot::ATTR_MODEL_TYPE, $char_length);
            $table->bigInteger(PolymorphablePivot::ATTR_CONTENT_ID);
            $table->string(PolymorphablePivot::ATTR_CONTENT_TYPE, $char_length);

            $table = Utilizer::utilize([
                Sequencer::class, Grouper::class
            ], PolymorphablePivot::class, $table);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists((new PolymorphablePivot)->getTable());
    }
};
